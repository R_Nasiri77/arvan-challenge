import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import { $axios } from '~/utils/api';
const Cookie = process.client ? require('js-cookie') : undefined;
@Module({
    name: 'user',
    stateFactory: true,
    namespaced: true,
})
class UserModule extends VuexModule {
    user: object = {}
    token: string = ''

    @Mutation
    setUser(user: object) {
        this.user = user
    };

    @Mutation
    setToken(token: string) {
        this.token = token
    };

    @Action({rawError: true })
    async loginUser(payload: { name: string, password: string }) {
        const res = await $axios.post('/users/login', { 
            user: {
                email: payload.name,
                password: payload.password
            }
        })
        this.setUser(res.data.user);
        Cookie.set('token', res.data.user.token);
        $axios.setHeader('Authorization', 'Token ' + res.data.user.token)
    }

}

export default UserModule;
