import { Store } from 'vuex'
import { initialiseStores } from '~/utils/store-accessor'
const initializer = (store: Store<any>) => initialiseStores(store);
const cookieparser = (process as any).server ? require('cookieparser') : undefined;

export const plugins = [initializer]
export * from '~/utils/store-accessor';


export const actions = {
    async nuxtServerInit(ctx: any, args: any) {
        if (args.req.headers.cookie) {
            const parsed = cookieparser.parse(args.req.headers.cookie);
            const token = 'Token ' + parsed.token;
            args.$axios.setToken(token)
        }
    }
}