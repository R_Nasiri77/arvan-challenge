import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import { $axios } from '~/utils/api';
import { Articles } from '../articles/types';
@Module({
    name: 'singleArticle',
    stateFactory: true,
    namespaced: true,
})
export default class SingleArticle extends VuexModule {
    [x: string]: any;
    tags: string[] = [];
    selectedTags: string[] = [];
    articleTitle: string = '';
    articleBody: string = '';
    articleDescription: string = '';

    @Mutation
    setTagsList(tags: string[]) {
        this.tags = tags
    }

    @Mutation
    addNewTag(tag: string) {
        this.tags.push(tag);
    }

    @Mutation
    addSelectedTag(tag: string) {
        const index = this.selectedTags.length ? this.selectedTags.indexOf(tag) : -1;
        if (index > -1) {
            this.selectedTags.splice(index, 1);
        } else {
            this.selectedTags.push(tag);
        }
    }

    @Mutation
    setSelectedTagsList(tags: string[]) {
        this.selectedTags = tags;
    }

    @Mutation
    setArticleTitle(title: string) {
        this.articleTitle = title;
    }

    @Mutation
    setArticleBody(body: string) {
        this.articleBody = body;
    }

    @Mutation
    setArticleDescription(description: string) {
        this.articleDescription = description;
    }

    @Action
    getTagsList() {
        $axios.$get('/tags').then((res) => {
            res.tags = res.tags.map((e: string) => e && e.replace(/[^a-zA-Z ]/g, ""));
            res.tags = res.tags.filter(function(item: string, pos: number) {
                return (item && item.length && res.tags.indexOf(item) == pos);
            })
            res.tags.sort((a: string, b: string) => a.localeCompare(b));
            this.setTagsList(res.tags);
        })
    }

    @Action
    addNewArticle() {
        const article = {
            title: this.articleTitle,
            description: this.articleDescription,
            body: this.articleBody,
            tagList: this.selectedTags
        } 
        $axios.$post('/articles', { article }).then(() => {
            this.store.$router.push({ name: 'articles', params: {title: 'created' }})
        })
    }

    @Action
    getArticleDetail(slug: string) {
        $axios.$get(`/articles/${slug}`).then((res) => {
            this.setArticleTitle(res.article.title);
            this.setArticleDescription(res.article.description);
            this.setArticleBody(res.article.body);
            this.setSelectedTagsList(res.article.tagList);
        })
    }

    @Action
    updateArticle(slug: string) {
        const article = {
            title: this.articleTitle,
            description: this.articleDescription,
            body: this.articleBody,
            tagList: this.selectedTags
        }
        $axios.$put(`/articles/${slug}`, { article }).then(() => {
            this.store.$router.push({ name: 'articles', params: {title: 'updated' }})
        })
    }

}

