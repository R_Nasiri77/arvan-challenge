import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import { $axios } from '~/utils/api';
import { Articles } from './types';
@Module({
    name: 'articles',
    stateFactory: true,
    namespaced: true,
})
export default class ArticlesModule extends VuexModule {
    articles: Articles = { articles: [], articlesCount: 0 }
    totalPages: number = 1;

    @Mutation
    setArticles(articles: Articles) {
        this.articles = articles
    }

    @Mutation
    setTotalPages(totalPages: number) {
        this.totalPages = totalPages
    }

    @Action
    getArticlesList(page: number) {
        const startIndex = page === 1 ? 1 : (page * 10) + 1;
        $axios.$get(`/articles?limit=10&&offset=${startIndex}`).then((res) => {
            this.setArticles(res);
            this.setTotalPages(res.articlesCount / 10);
        })
    }

    @Action
    async removeArticle(slug: number) {
       await $axios.$delete(`articles/${slug}`)
    }
}

