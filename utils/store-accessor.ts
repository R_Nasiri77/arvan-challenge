import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import UserModule from '~/store/user'
import ArticlesModule from '~/store/articles/index'

let userStore: UserModule
let articlesModule: ArticlesModule

function initialiseStores(store: Store<any>): void {
  userStore = getModule(UserModule, store)
  articlesModule = getModule(ArticlesModule, store)
}

export { initialiseStores, userStore, articlesModule }