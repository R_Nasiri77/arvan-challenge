export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "arvan-challenge",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~plugins/ag-grid.js', ssr: false },
    '~plugins/axios-accessor.ts'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
  ],
  typescript: {
    typeCheck: true,
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    "bootstrap-vue/nuxt",
    "@nuxtjs/axios",
    "@nuxtjs/auth",
    "@nuxtjs/style-resources",
  ],
  axios: {
    baseURL: "https://conduit.productionready.io/api/",
  },
  auth: {
    strategies: {
      local: {
        token: {
          property: '',
          global: true,
          required: true,
        },
        tokenType: 'Token',
        endpoints: {
          login: { url: "users/login", method: "post", propertyName: "user.token" },
          user: { url: "user", method: "get" },
        },
      },
    },
  },
  styleResources: {
    scss: ["~assets/scss/config.scss"],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~assets/scss/config.scss", "~assets/scss/common.scss"],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
  },
  router: {
		extendRoutes(routes, resolve) {
      routes.push(
        {
					path: '/articles',
					name: 'articles',
					component: resolve(__dirname, 'pages/articles/index.vue')
				},
        {
					path: '/articles/page/:pageNumber',
					name: 'articles',
					component: resolve(__dirname, 'pages/articles/index.vue')
				},
        {
					path: '/articles/edit/:slug',
					name: 'singleArticle',
					component: resolve(__dirname, 'pages/articles/create.vue')
				},
      )
    },
    middleware: 'checkAuth'
	},
};
