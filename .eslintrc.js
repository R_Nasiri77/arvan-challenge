module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: [
    'prettier',
    'prettier/vue',
    '@nuxtjs',
    'plugin:nuxt/recommended',
    '@nuxtjs/eslint-config-typescript',
    "eslint:recommended"
  ],
  plugins: [
	  'prettier'
  ],
  // add your custom rules here
  rules: {
    "indent": ["error", "tab", { "SwitchCase": 1 }],
    "no-tabs": 0,
    "vue/html-indent": ["error", "tab"],
    "semi": "off",
    "@typescript-eslint/semi": ["error", "always"],
    "no-console": "off",
    "vue/no-v-html": "off",
    "space-before-function-paren": ["error", {
      "anonymous": "always",
      "named": "never",
      "asyncArrow": "always"
    }],
    "curly": ["error", "multi-line"],
    "default-case-last": "off",
    "no-useless-backreference": "off",
    "no-loss-of-precision": "off",
    "no-unreachable-loop": "off"
  }
}